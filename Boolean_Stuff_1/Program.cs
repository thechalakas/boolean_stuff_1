﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boolean_Stuff_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //these operator stuff is something you know anyway. But, lets demonstrate it anyway.

            //lets accept two numbers from the user
            Console.WriteLine(" Enter the first number");
            string s = Console.ReadLine();
            Console.WriteLine(" Enter the second number");
            string s2 = Console.ReadLine();
            int number_one, number_two;
            number_one = Convert.ToInt32(s);
            number_two = Convert.ToInt32(s2);


            Console.WriteLine("The two numbers are {0} and {1}", number_one, number_two);
            //less than 
            if(number_one < number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "less than", number_two);
            }
            //greater than 
            if (number_one > number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "greater than", number_two);
            }
            //less than or equal to
            if (number_one <= number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "less than or equal to", number_two);
            }
            //greater than or equal to
            if (number_one >= number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "greater than or equal to", number_two);
            }
            //equal to
            if (number_one == number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "equal to", number_two);
            }
            //not equal to
            if (number_one != number_two)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "not equal to", number_two);
            }

            //so, all the relationa and equality operators are done

            //lets try the AND opearator
            if (number_one %2 == 0 && number_two % 2 == 0)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "are both even numbers ", number_two);
            }

            //lets try the OR operator
            if (number_one % 2 ==0 ||  number_two % 2 ==0)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "at least one of the or both of them are even", number_two);
            }

            //lets try the XOR operator when one of the items is right. 
            if (number_one % 2 == 0 ^ number_two % 2 == 0)
            {
                Console.WriteLine(" {0} is {1} {2}", number_one, "exactly one of them is even", number_two);
            }

            //avoiding the console window from dissapearing
            Console.ReadLine();
        }
    }
}
